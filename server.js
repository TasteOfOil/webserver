const http = require('http');
const path = require('path');
const fs = require('fs');


const PORT = 3000;
const HOST = 'localhost';

let indexFile;

const reqListener = (req,res)=>{
    
switch(req.url){
    case "/":
        fs.readFile(path.join(__dirname,"public","index.html"), (err, content)=>{
            if(err) {
                res.writeHead(500);
                res.end(err);
                return;
            }
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200);
            console.log("Start page");
            res.end(content);
        });
        break;
    case "/image":
        fs.readFile(path.join(__dirname,"public","cat.jpg"), (err, content)=>{
            if(err) {
                res.writeHead(500);
                res.end(err);
                return;
            }
            res.setHeader("Content-Type", "image/jpeg");
            res.writeHead(200);
            console.log("Image");
            res.end(content);
        });
        break;
    case "/css":
        fs.readFile(path.join(__dirname,"public","style.css"), (err, content)=>{
            if(err) {
                res.writeHead(500);
                res.end(err);
                return;
            }
            res.setHeader("Content-Type", "text/css");
            res.writeHead(200);
            console.log("Css file");
            res.end(content);
        });
        break;
    default:
        res.writeHead(500);
        res.end("Error 404: Page not found");
        break;
}
    
}



const server = http.createServer(reqListener);
server.listen(PORT, HOST, ()=>{
    console.log("Server listening..");
   
});




